/**
 * Created by Gary on 6/22/2015.
 */

var mysql = require('mysql');
var validator = require('validator');
var lsAPI = require('./ls_api.js');

var Director = require("./director.js");

function Routes() {

}

// create director (POST request to http://localhost:PORT(8080)/DirectorsAPI/directors)
Routes.addDirector = function(req, res, pool){
    pool.getConnection(function(err, connection) {
        // check for bad input...
        if (validator.isNumeric(req.body.livestream_id) && req.body.livestream_id.length == 7) {
            // grab account details via Livestream API and create the director with the callback
            lsAPI.getAccountDetails(req.body.livestream_id, res, connection, Routes.createDirector);
        }
        else {
            // send back informative message in case of improper livestream_id format
            res.json({message: "Livestream ID must consist of 7 digits."});
            connection.release();
        }
    })
};

// create director (POST request to http://localhost:PORT(8080)/DirectorsAPI/directors)
// only need to provide a "livestream_id" value in POST request
Routes.createDirector = function (details, res, connection) {

    if(details.length == 0) { // something went awry in Livestream API -> inform user and relinquish MySQL connection
        res.json({message: "Cannot connect to Livestream account. Please verify correct Livestream ID or try again later."});
        connection.release();
        return;
    }

    var query = "SELECT full_name FROM test_directors WHERE full_name = ?";
    var values = [details[0]];
    query = mysql.format(query, values);
    connection.query(query, function(err, rows) {
        if(!err && rows.length > 0) { // director already exists, abort creation
            res.json({message: "This director already exists in the database."});
            connection.release();
        }
        else if(!err && rows.length == 0) {
            var director = new Director();
            director.full_name = details[0];
            director.dob = details[1];

            // store in MySQL database
            query = "INSERT INTO test_directors VALUES (DEFAULT, ?, ?, DEFAULT)";
            values = [director.full_name, director.dob];
            query = mysql.format(query, values);
            connection.query(query, function (err, rows) {
                if (!err) {
                    res.json({message: "Director added to database with ID=" + rows.insertId + "!", director: director});
                }
                else {
                    res.json({message: "Failed to add director to database.", error: err});
                }
                connection.release();
            });
        }
        else {
            console.log(err);
            res.json({message: "Failed to add director to database.", error: err});
            connection.release();
        }
    });


};

// retrieve all directors if no ID is given as param
Routes.getDirectors = function (req, res, pool) {
    pool.getConnection(function(err, connection) {
        // get all scalar info about a director
        var query = "SELECT test_directors.id, full_name, dob, camera_name AS favorite_camera FROM test_directors " +
            "LEFT JOIN test_cameras ON test_directors.favorite_camera = test_cameras.id";
        var directorList = [];
        connection.query(query, function (err, rows) {
            if (!err && rows.length > 0) {
                for (var i = 0; i < rows.length; i++) {
                    var directorObj = new Director();
                    for (var prop in rows[i]) {
                        if (prop != 'id') {
                            directorObj[prop] = rows[i][prop];
                        }
                    }
                    // now get the movies
                    Routes.retrieveMovies(res, connection, rows[i].id, directorList, directorObj, rows.length, i, Routes.linkMovies);
                }
            }
            else if (rows.length == 0) {
                res.json({message: "No directors on record."})
                connection.release();
            }
            else {
                res.json({message: "Failed to retrieve directors.", error: err});
                connection.release();
            }
        });
    })
};

// get specific director (GET request to http://localhost:PORT(8080)/DirectorsAPI/directors/:id)
Routes.getSpecificDirector = function(req, res, pool) {
    pool.getConnection(function(err, connection) {
        var query = "SELECT test_directors.id, full_name, dob, camera_name AS favorite_camera FROM test_directors " +
            "LEFT JOIN test_cameras ON test_directors.favorite_camera = test_cameras.id " +
            "WHERE test_directors.id = ?";
        var values = [req.params.id];
        query = mysql.format(query, values);
        var directorList = [];
        connection.query(query, function (err, rows) {
            if (!err && rows.length > 0) {
                var directorObj = new Director();
                for (var prop in rows[0]) {
                    if (prop != 'id') {
                        directorObj[prop] = rows[0][prop];
                    }
                }
                // now get the movies
                Routes.retrieveMovies(res, connection, rows[0].id, directorList, directorObj, rows.length, 0, Routes.linkMovies);
            }
            else if (!err && rows.length == 0) {
                res.json({
                    message: "Failed to retrieve director with ID=" + req.params.id,
                    error: "There is no director with ID=" + req.params.id
                });
                connection.releaseConnection();
            }
            else {
                res.json({message: "Failed to retrieve director with ID=" + req.params.id, error: err});
                connection.releaseConnection();
            }
        })
    })
};

// update director (PUT request to http://localhost:PORT(8080)/DirectorsAPI/directors/:id)
// requires http header of "content-type=application/json" with "favorite_camera" being a scalar value and "favorite_movies" being an array of movies
Routes.updateDirector = function(req, res, pool) {
    pool.getConnection(function(err, connection) {
        if (req.body.favorite_camera && req.body.favorite_movies) {
            Routes.updateCamera(req, res, connection); // update camera
            Routes.updateMovies(req, res, connection, function(id) { // update movies then send response to user
                res.json({message: "Director with ID=" + id + " had changes successfully made to their favorite camera and movies!"})
                connection.release();
            });
        }
        else if (!req.body.favorite_camera && !req.body.favorite_movies) {
            res.json({message: "Neither a camera nor a list of favorite movies was provided; no change was made to director with ID=" + req.params.id});
            connection.release();
        }
        else if (!req.body.favorite_movies) {
            Routes.updateCamera(req, res, connection, function(id) {
                res.json({message: "Director with ID=" + req.params.id + " had changes successfully made to their favorite camera!"});
                connection.release();
            });
        }
        else {
            Routes.updateMovies(req, res, connection, function(id) {
                res.json({message: "Director with ID=" + req.params.id + " had changes successfully made to their favorite movies!"});
                connection.release();
            });
        }
    })
};

// delete director (DELETE request to http://localhost:PORT(8080)/DirectorsAPI/directors/:id)
Routes.deleteDirector = function(req, res, pool) {
    pool.getConnection(function(err, connection) {
        // begin transaction of removing director and all entries for that director in intermediate directors/movies table
        connection.beginTransaction(function (err) {
            var query = "DELETE FROM test_directors_movies WHERE director_id = ?";
            var values = [req.params.id];
            query = mysql.format(query, values);
            connection.query(query, function (err) {
                if (err) {
                    connection.rollback(function (err) {
                        throw err;
                    });
                }
                else {
                    query = "DELETE FROM test_directors WHERE id = ?";
                    query = mysql.format(query, values); // value of director id remains unchanged
                    connection.query(query, function (err, rows) {
                        if (err) {
                            connection.rollback(function (err) {
                                throw err;
                            });
                        }
                        else {
                            connection.commit(function (err) {
                                if (err) {
                                    throw err;
                                }
                                else {
                                    if (rows.affectedRows != 0) {
                                        res.json({message: "Director with ID=" + req.params.id + " deleted!"});
                                    }
                                    else {
                                        res.json({message: "Director with ID=" + req.params.id + " does not exist in database."});
                                    }
                                }
                            })
                        }
                    });
                }
            });
        });
    })
};

// function to make appropriate camera updates/inserts to MySQL database, all taken as one transaction should an error occur
Routes.updateCamera = function(req, res, connection, callback) {
    connection.beginTransaction(function (err) {
        // check to see if favorite_camera supplied in request already exists in DB
        var query = "SELECT * FROM test_cameras WHERE camera_name = ?";
        var values = [req.body.favorite_camera];
        query = mysql.format(query, values);
        connection.query(query, function (err, rows) {
            if (err) { // if there's an error, rollback transaction
                connection.rollback(function () {
                    throw err;
                });
            }
            else if (rows.length == 0) { // if no such camera exists in DB, create an entry for it
                query = "INSERT INTO test_cameras VALUES (DEFAULT, ?)";
                query = mysql.format(query, req.body.favorite_camera);
                connection.query(query, function (err, rows) {
                    if (err) {
                        connection.rollback(function () { // if there's an error, rollback transaction
                            throw err;
                        });
                    }
                    else { // after camera has been added, link it to director by updating foreign key
                        query = "UPDATE test_directors SET favorite_camera = ? WHERE id = ?";
                        var values = [rows.insertId, req.params.id]; // grab the auto_increment ID we got when inserting camera entry
                        query = mysql.format(query, values);
                        connection.query(query, function (err, rows) {
                            if (err) {
                                connection.rollback(function () {
                                    throw err;
                                })
                            }
                            else {
                                connection.commit(function (err) {
                                    if (err) {
                                        connection.rollback(function () {
                                            throw err;
                                        });
                                    }
                                    else if (callback) {
                                        callback(req.params.id);
                                    }
                                });
                            }
                        });

                    }
                });
            }
            else { // the camera exists in DB: grab it from result set and link to director's foreign key
                query = "UPDATE test_directors SET favorite_camera = ? WHERE id = ?";
                var values = [rows[0].id, req.params.id]; // grab ID of camera from result set and ID of director from request params
                query = mysql.format(query, values);
                connection.query(query, function (err, rows) {
                    if (err) {
                        connection.rollback(function () {
                            throw err;
                        });
                    }
                    else {
                        connection.commit(function (err) {
                            if (err) {
                                connection.rollback(function () {
                                    throw err;
                                });
                            }
                            else if (callback) {
                                callback(req.params.id); // inform user of update after success
                            }
                        });
                    }
                });
            }
        })
    });
};

// function to make appropriate movie updates/inserts to MySQL database, all taken as one transaction should an error occur
Routes.updateMovies = function(req, res, connection, callback) {
    connection.beginTransaction(function (err) {
        var movieList = req.body.favorite_movies;
        for (var i = 0; i < movieList.length; i++) {
            (function (i) { // begin closure for asynchronous queries that utilize loop iterator 'i'...
                //check to see if the movies already exist in DB
                var query = "SELECT * FROM test_movies WHERE title = ?";
                var values = [movieList[i]];
                query = mysql.format(query, values);
                connection.query(query, function (err, rows) {
                    if (err) {
                        connection.rollback(function (err) {
                            throw err;
                        });
                    }
                    else if (rows.length == 0) { // if the movie doesn't exist in DB, create entry for it
                        query = "INSERT INTO test_movies VALUES (DEFAULT, ?)";
                        query = mysql.format(query, movieList[i]);
                        connection.query(query, function (err, rows) {
                            if (err) {
                                connection.rollback(function (err) {
                                    throw err;
                                });
                            }
                            else { // after movie has been added, link it to director by adding entry to intermediate table
                                query = "INSERT IGNORE INTO test_directors_movies VALUES (?, ?)";
                                var values = [req.params.id, rows.insertId]; // grab the auto_increment ID we got when inserting movie entry
                                query = mysql.format(query, values);
                                connection.query(query, function (err, rows) {
                                    if (err) {
                                        connection.rollback(function () {
                                            throw err;
                                        })
                                    }
                                    else {
                                        connection.commit(function (err) {
                                            if (err) {
                                                connection.rollback(function () {
                                                    throw err;
                                                });
                                            }
                                            else if(callback) {
                                                callback(req.params.id);
                                            }
                                        });
                                        connection.release();
                                    }
                                });
                            }
                        });
                    }
                    else { // the movie exists in DB; grab it from result set and link to director by adding entry to intermediate table
                        query = "INSERT IGNORE INTO test_directors_movies VALUES (?, ?)";
                        var values = [req.params.id, rows[0].id]; // grab the auto_increment ID we got when inserting movie entry
                        query = mysql.format(query, values);
                        connection.query(query, function (err, rows) {
                            if (err) {
                                connection.rollback(function () {
                                    throw err;
                                })
                            }
                            else {
                                connection.commit(function (err) {
                                    if (err) {
                                        connection.rollback(function () {
                                            throw err;
                                        });
                                    }
                                    else if(callback){
                                        callback(req.params.id);
                                    }
                                });
                                connection.release();
                            }
                        });
                    }
                })
            })(i); // end closure for asynchronous queries that utilize loop iterator 'i'...
        }
    });
};

// retrieves favorite movies of director from MySQL database based on ID passed into it from getDirectors function
Routes.retrieveMovies = function(res, connection, id, directorList, directorObj, numDirectors, currentIndex, linkMoviesCallback) {
    // query to link directors to movies via normalized intermediate database class
    var query = "SELECT title FROM test_movies " +
    "JOIN test_directors_movies ON test_movies.id = test_directors_movies.movie_id " +
    "JOIN test_directors ON test_directors_movies.director_id = test_directors.id " +
    "WHERE test_directors.id = ?";
    var values = [id];
    var movies = [];
    query = mysql.format(query, values);
    connection.query(query, function (err, rows) {
        if (!err && rows.length > 0) {
            for (var j = 0; j < rows.length; j++) {
                movies.push(rows[j].title);
            }
        }
            linkMoviesCallback(directorObj, directorList, movies, function(directorList) {
                // will only send response if the current director is the last in the resultset returned in getDirectors function
                if(currentIndex == numDirectors - 1) {
                    res.json({message: "Retrieved director(s)!", directors: directorList});
                    connection.release();
                }
            });
    });
};

// callback used in retrieve movies to avoid problems due to async calls.
Routes.linkMovies = function(directorObj, directorList, movies, callback) {
    directorObj.favorite_movies = movies;
    directorList.push(directorObj);
    callback(directorList);
};

module.exports = Routes;