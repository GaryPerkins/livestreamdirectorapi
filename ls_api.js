/**
 * Created by Gary on 6/17/2015.
 */

function LivestreamAPI() {

}

LivestreamAPI.getAccountDetails = function(livestreamID, resObj, connObj, directorCreationCallback) {
    var request = require('request');
    var detailsOfInterest = [];
    request('https://api.new.livestream.com/accounts/' + livestreamID, function(err, res, body) {
        if(!err && res.statusCode == 200) {
            try {
                var fullDetails = JSON.parse(body);
                detailsOfInterest.push(fullDetails.full_name);
                detailsOfInterest.push(fullDetails.dob);
            }catch(err) {
                console.log(err);
            }

            directorCreationCallback(detailsOfInterest, resObj, connObj);
        }
        else if(!err && res.statusCode == 500) {
            console.log("Got status code 500 from Livestream API");
            directorCreationCallback(detailsOfInterest, resObj, connObj);
        }
        else {
            console.log(err);
            directorCreationCallback(detailsOfInterest, resObj, connObj);
        }
    });
};

module.exports = LivestreamAPI;