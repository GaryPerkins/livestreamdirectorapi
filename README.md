# Livestream Director API #

API for registering/retrieving/updating/deleting directors from normalized MySQL database via HTTP requests. Test it out using your favorite REST client!

## API root URL (rootURL): localhost:PORT/DirectorsAPI/ ##
* GET requests: (rootURL)/directors to retrieve all directors in database  (rootURL)/directors/:id to retrieve director based on id provided.
* POST requests: (rootURL)/directors with "livestream_id" value within request body to automatically register an account via details grabbed from Livestream API.
* PUT requests: (rootURL)/directors/:id with either (or both) "favorite_camera"(scalar value) or "favorite_movies"(array of scalar values) to update their respective values for the director with that id. Request header must contain "content-type" of "application/json".
* DELETE requests: (rootURL)/directors/:id to delete the director with the provided ID as well as any rows in the database that depend upon them.
* Tested with Postman Chrome extension.

## Dependencies ##

* Express (v. ~4.0.0)
* Node MySQL (v. ~2.6.2)
* Body-Parser (v. ~1.0.2)
* Validator (latest version)

Simply run "npm update" from project's root directory to install dependencies via included package.json file.
### Comments/Questions? ###
gary.perkins1164@gmail.com