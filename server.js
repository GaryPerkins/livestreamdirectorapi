/**
 * Created by Gary on 6/16/2015.
 */

// get packages
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var routes = require('./routes.js');

// set up MySQL db connection
var mysql = require('mysql');

// MySQL DB connection parameters; change as needed
var MYSQL_PORT = 3306,
    MYSQL_USER = 'root',
    MYSQL_PW = 'student',
    MYSQL_DB = 'director_api';

// using connection pool to handle simultaneous DB usage
var pool = mysql.createPool({ // using default port value of 3306
    connectionLimit: 100,
    host: 'localhost',
    user: MYSQL_USER,
    password: MYSQL_PW,
    database: MYSQL_DB
});

pool.on('connection', function(connection) {
    console.log("Connection made with id " + connection.threadId);
});

// allow app to use bodyParser in order to retrieve data from POST
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json({strict: false}));

// set the port
var PORT = 8080;

// grab express Router
var router = express.Router();

// ------------- ROUTES ----------------------

app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.status(500).send('Something broke!');
});

// middleware for all requests
router.use(function(req, res, next) {
    // log
    console.log("**************************\nRequest made.\nTime: " + Date.now());
    console.log("Request Type: " + req.method);
    next();
});

// routes that end in /directors...
router.route('/directors')

    // create director (POST request to http://localhost:PORT(8080)/DirectorsAPI/directors)
    .post(function(req, res) {
        routes.addDirector(req, res, pool);
    })

    // retrieve all directors if no ID is given as param
    .get(function(req, res) {
        routes.getDirectors(req, res, pool);
    });

// for routes where an ID is supplied as a param...
router.route('/directors/:id')

    // get specific director (GET request to http://localhost:PORT(8080)/DirectorsAPI/directors/:id)
    .get(function(req, res) {
        routes.getSpecificDirector(req, res, pool);
    })

    // update director (PUT request to http://localhost:PORT(8080)/DirectorsAPI/directors/:id)
    .put(function(req, res) {
        routes.updateDirector(req, res, pool);
    })

    // delete director (DELETE request to http://localhost:PORT(8080)/DirectorsAPI/directors/:id)
    .delete(function(req, res) {
        routes.deleteDirector(req, res, pool);
    });


// set route prefix
app.use('/DirectorsAPI', router);

// start server
app.listen(PORT);
console.log('Server running on port ' + PORT);