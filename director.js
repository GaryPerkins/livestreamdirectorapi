/**
 * Created by Gary on 6/16/2015.
 */

function Director() {
    this.full_name = "";
    this.dob = "";
    this.favorite_camera = "";
    this.favorite_movies = [];
}

//// overloaded constructor for testing purposes
//function Director(full_name, dob, favorite_camera, favorite_movies) {
//    this.full_name = full_name;
//    this.dob = dob;
//    this.favorite_camera = favorite_camera;
//    this.favorite_movies = favorite_movies;
//}

module.exports = Director;